####Install dependencies
```console
composer i
npm i
```

####Create symlink
```console
php artisan storage:link
```
More details: https://laravel.com/docs/7.x/filesystem#the-public-disk

Created with documentation http://api.programator.sk/
