<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['path', 'name'];

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function getFirstImage()
    {
        $image = $this->images()->first();
        if (is_null($image)) {
            return false;
        }
        return new \App\Http\Resources\Image($image);
    }

}
