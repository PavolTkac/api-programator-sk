<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Resources\Gallery as GalleryResource;
use App\Http\Resources\Image as ImageResource;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = [
          'galleries' => GalleryResource::collection(Gallery::all()),
        ];

        return response()->json($galleries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = ['name' => 'required'];

        $messages = [
            'required' => "Bad JSON object: u'name' is a required property",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        preg_match('/\//', $request->input('name'), $matches);

        if ($validator->fails() || count($matches)) {
            $description = '';
            if ($validator->getMessageBag()->isNotEmpty()) {
                $description = $validator->getMessageBag()->first();
            } else {
                $description = "Bad JSON object: u'name' must not contain '/' character.";
            }
            return response()->json([
                'code' => 400,
                'payload' => [
                    'paths' => ['name'],
                    'validator' => 'required',
                    'example' => null,
                ],
                'name' => 'INVALID_SCHEMA',
                'description' => $description
            ], 400);
        }

        $existGallery = Gallery::where('name', $request->input('name'))->first();
        if (isset($existGallery)) {
            return response(null, 409);
        }

        $gallery = Gallery::create([
            'name' => $request->input('name'),
            'path' => rawurlencode($request->input('name'))
        ]);

        return response()->json(new GalleryResource($gallery), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $path)
    {
        $gallery = Gallery::where('path', rawurlencode($path))->first();
        $responseData = [];
        if (is_null($gallery)) {
            return response(null, 404);
        } else {
            $responseData = [
                'gallery' => (new GalleryResource($gallery))->withoutImage(),
                'images' => ImageResource::collection($gallery->images()->get())
            ];
        }

        return response()->json($responseData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($path)
    {
        $gallery = Gallery::where('path', rawurlencode($path))->first();
        $image = Image::where('path', rawurlencode($path))->first();

        if (isset($gallery)) {
            $this->deleteGallery($gallery);
        }
        if (isset($image)){
            $this->deleteImage($image);
        }
        if (is_null($gallery) && is_null($image)) {
            return response(null, 404);
        }

        return response(null, 200);
    }

    private function deleteGallery($gallery)
    {
        $images = $gallery->images()->get();
        foreach ($images as $image) {
            Storage::delete(env('IMAGES_ROOT').'/'.$image->path);
        }
        $gallery->delete();
    }

    private function deleteImage($image)
    {
        Storage::delete(env('IMAGES_ROOT').'/'.$image->path);
        $image->delete();
    }

}
