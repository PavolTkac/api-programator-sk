<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image as ImageIntervention;
use App\Http\Resources\Image as ImageResource;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $path)
    {
        $gallery = Gallery::where('path', rawurlencode($path))->first();
        if (is_null($gallery)) return response(null, 404);

        $rules = ['image' => 'required|mimes:jpeg,jpg,png,gif'];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response(null, 400);
        }

        $fullFileName = $request->file('image')->getClientOriginalName();

        $existsFileName = Storage::disk(env('FILESYSTEM_DRIVER'))->exists(env('IMAGES_ROOT').'/'.$fullFileName);

        while($existsFileName) {
            $fileName = pathinfo($fullFileName, PATHINFO_FILENAME);
            $fileExtension = $request->file('image')->getClientOriginalExtension();
            $fullFileName = $fileName.generateRandomString().'.'.$fileExtension;
            $existsFileName = Storage::disk(env('FILESYSTEM_DRIVER'))->exists(env('IMAGES_ROOT').'/'.$fullFileName);
        }

        $filePath = $request->file('image')->storeAs(env('IMAGES_ROOT'),$fullFileName);

        $image = $gallery->images()->create([
            'path' => $fullFileName,
            'fullpath' => $gallery->path.'/'.$fullFileName,
            'name' => pathinfo($fullFileName, PATHINFO_FILENAME),
        ]);

        return response()->json([
            'uploaded' => new ImageResource($image),
        ], 201);
    }

    public function show($width, $height, $galleryPath, $imagePath)
    {
        $image = Image::where('fullpath', $galleryPath.'/'.$imagePath)->first();

        if (is_null($image)) return response(null, 404);

        $file = Storage::get(env('IMAGES_ROOT').'/'.$image->path);

        $fileImage = ImageIntervention::make($file);
        $this->processImage($fileImage, $width, $height);

        return $fileImage->response();
    }

    private function processImage($fileImage, $width, $height)
    {
        if ((int) $width === 0 && (int) $height > 0) {
            $fileImage->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        if ((int) $height === 0 && (int) $width > 0) {
            $fileImage->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        if ((int) $width > 0 && (int) $height > 0) {
            if ($fileImage->width() < $width || $fileImage->height() < $height) {
                $tmpWidth = null;
                $tmpHeight = null;

                $fileImage->widen($width);
                if ($fileImage->height() < $height) {
                    $fileImage->heighten($height);
                }
                $fileImage->crop($width, $height, 0, 0);
            } else {
                $fileImage->crop($width, $height, 0, 0);
            }
        }
    }
}
