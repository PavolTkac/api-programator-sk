<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Gallery extends JsonResource
{
    private $withImage = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'path' => $this->path,
            'name' => $this->name,
            'image' => $this->when($this->getFirstImage() && $this->withImage, $this->getFirstImage())
        ];
    }

    public function withoutImage()
    {
        $this->withImage = false;
        return $this;
    }
}
