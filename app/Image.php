<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['path', 'fullpath', 'name'];

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }
}
