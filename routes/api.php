<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware('api')->get('/gallery', 'GalleryController@index')->name('gallery.index');
Route::middleware('api')->post('/gallery', 'GalleryController@store')->name('gallery.store');

Route::middleware('api')->get('/gallery/{path}', 'GalleryController@show')->name('gallery.show');
Route::middleware('api')->post('/gallery/{path}', 'ImageController@store')->name('image.store');
Route::middleware('api')->delete('/gallery/{path}', 'GalleryController@destroy')->name('gallery.destroy');

Route::middleware('api')->get('/images/{width}x{height}/{galleryPath}/{imagePath}', 'ImageController@show')->name('image.show');
